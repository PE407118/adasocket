<?php

/**
 * @package   block_adasocket
 * @copyright 2019, Omar Terrazas Razo <omar_terrazas@cuaed.unam.mx>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

function get_userlasttrack() {
    global $USER, $DB;
 
    $query = 'SELECT * FROM mdl_adasocket WHERE userid = ' . $USER->id . ' ORDER BY id DESC LIMIT 1';
    return $DB->get_record_sql($query);
}

function get_usertracks($courseid) {
    global $USER, $DB;
 
    return $DB->get_records('adasocket', array('userid' => $USER->id, 'courseid' => $courseid));
}

function set_userrecord($courseid, $currenturl) {
    global $USER, $DB;

    $groupid = groups_get_user_groups($courseid, $USER->id);

    $urlsegments = explode('/',$currenturl);
    $segmentindex = 0;
    foreach ($urlsegments as $segment) {
        $segmentindex ++;
        if($segment === 'content') {
            break;
        }
    }
    $segmentsurl = array_slice($urlsegments,$segmentindex + 1);
    $contentslug = '';
    foreach ($segmentsurl as $segment) {
        $contentslug .= '/' . $segment;
    }

    $now = new DateTime("now", core_date::get_server_timezone_object());
    $timecreated = $now->getTimestamp();

    $dataobject = array('userid' => $USER->id, 'groupid' => $groupid[0][0], 'courseid' => $courseid, 'contenturl' => $currenturl, 'contentslug' => $contentslug, 'timecreated' => $timecreated);

    return $DB->insert_record('adasocket', $dataobject, $returnid=true, $bulk=false);
}
