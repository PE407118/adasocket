<?php

/**
 * @package   block_adasocket
 * @copyright 2019, Omar Terrazas Razo <omar_terrazas@cuaed.unam.mx>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Execute adasocket upgrade from the given old version
 *
 * @param int $oldversion
 * @return bool
 */

function xmldb_block_adasocket_upgrade($oldversion, $block) {
    global $CFG;

    return true;
}
