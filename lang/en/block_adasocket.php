<?php

/**
 * @package   block_adasocket
 * @copyright 2019, Omar Terrazas Razo <omar_terrazas@cuaed.unam.mx>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'ADA Socket';
$string['block_adasocket'] = 'ADA Socket';
$string['adasocket'] = 'ADA Tracking';
$string['viewlast'] = 'Go to last content';
$string['noviewlast'] = 'There is no previous activity';
$string['here'] = 'here';
$string['viewrecord'] = 'View record';
