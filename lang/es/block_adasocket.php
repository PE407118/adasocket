<?php

/**
 * @package   block_adasocket
 * @copyright 2019, Omar Terrazas Razo <omar_terrazas@cuaed.unam.mx>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'ADA Socket';
$string['block_adasocket'] = 'ADA Socket';
$string['adasocket'] = 'ADA Seguimiento';
$string['viewlast'] = 'Ir al último contenido consultado';
$string['noviewlast'] = 'No existe actividad previa';
$string['here'] = 'aquí';
$string['viewrecord'] = 'Ver registro';
