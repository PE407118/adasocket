<?php

/**
 * @package   block_adasocket
 * @copyright 2019, Omar Terrazas Razo <omar_terrazas@cuaed.unam.mx>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require('../../config.php');
require_once(dirname(__FILE__).'/lib.php');

$courseid = required_param('courseid', PARAM_INT);

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if(isset($_POST['action'])) {
        if($_POST['action'] == 'record') {
            $record = set_userrecord($courseid, $_POST['url']);
            if($record) {
                echo json_encode(array('status' => 'SCS', 'recordid' => $record));
            } else {
                echo json_encode(array('status' => 'ERR'));
            }
        }
    } else {
        echo json_encode(array('message' => 'Welcome to nothingness'));
    }
} else {
    $url = new moodle_url('/blocks/adasocket/index.php', array('id'=>$courseid));
    $PAGE->set_url($url);
    $PAGE->set_pagelayout('admin');

    if (!$course = $DB->get_record('course', array('id'=>$courseid))) {
        print_error('invalidcourse');
    }

    require_login($course);

    $context = context_course::instance($course->id);
    $pluginname = get_string('adasocket', 'block_adasocket');

    $PAGE->set_title($course->shortname .': '. $pluginname);
    $PAGE->set_heading($course->fullname);
    $PAGE->set_pagetype('course-view-' . $course->format);
    $PAGE->add_body_class('path-user');                     // So we can style it independently.
    $PAGE->set_other_editing_capability('moodle/course:manageactivities');

    echo $OUTPUT->header();

    echo '<h2>' . $pluginname . '</h2>';

    $tracks = get_usertracks($course->id);

    if(count($tracks) > 0) {
        echo '<table><thead><tr><th>url</th><th>date</th></tr></thead><tbody>';
        foreach($tracks as $track) {
            echo '<tr><td><a href="' . $track->contenturl . '">' . $track->contentslug . '</a></td><td>' . userdate($track->timecreated, $format = '%B %d %Y, %H:%M:%S') . '</td></tr>';
        }
        echo '</tbody></table>';
    } else {
        echo '<h4>Aún no has explorado los contenidos.</h4>';
    }
    

    echo $OUTPUT->footer();
}
