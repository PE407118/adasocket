# ![](./pix/icon.png "ADA Socket") ADA Socket

Plugin de módulo Moodle para gestionar la identidad del usuario y almacenar mensajes.

## Instalación

Instalar como módulo de bloque de la plataforma y agregar el bloque **ADA Socket** dentro del curso.


## Tracking de contenido

Para registrar la navegación del usurio únicamente hay que agregar en el archivo `script.js` el siguiente bloque de código:

    /**
    * ADA Socket
    * @param {string} socketURL - URL of ADA Socket index file
    */
    $(function () {
        const socketURL = '';
        $.ajax({
            url: socketURL,
            method: 'post',
            data: {
                'action': 'record',
                'url': window.location.href
            },
        }).done(function (response) {
            console.log(response.recordid);
        }).fail(function (jqXHR, textStatus) {
            console.log("Request failed: " + textStatus);
        });
    });
