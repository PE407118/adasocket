<?php

/**
 * @package   block_adasocket
 * @copyright 2019, Omar Terrazas Razo <omar_terrazas@cuaed.unam.mx>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__).'/lib.php');

class block_adasocket extends block_base {
    public function init() {
        $this->title = get_string('adasocket', 'block_adasocket');
    }

    public function get_content() {
        global $USER, $CFG;

        if ($this->content !== null) {
          return $this->content;
        }
        
        $lasttrack = get_userlasttrack();

        $this->content         = new stdClass;
        if(isset($lasttrack->id)) {
            $this->content->text   = get_string('viewlast', 'block_adasocket') . ' <a href="' . $lasttrack->contenturl . '">' . get_string('here', 'block_adasocket') . '</a>.';
        } else {
            $this->content->text   = get_string('noviewlast', 'block_adasocket');
        }
        $this->content->footer = '<a href="'.$CFG->wwwroot.'/blocks/adasocket/index.php?courseid=' . $this->page->course->id . '">' . get_string('viewrecord', 'block_adasocket') . '</a>';
     
        return $this->content;
    }
}
